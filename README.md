# How to use

1. Make sure that folder pg_data exists
2. Make sure that .env file filled correctly
3. Run docker-compose up command to start server and db

To process payment, you should send POST request to this endpoint:
```
http://localhost:${PORT}/order/payment
```

Request Body:
```
{
    "token":"",
    "amount":,
    "user_id":,
    "order_id":
}
```

Server will find user by id, and try to make charge with his stripe customer id and payment source or will create them if they arent present.