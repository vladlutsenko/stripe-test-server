import express from 'express';
import * as controller from './controller'

const router = express.Router();


router.post('/login', controller.loginUser);

router.post('/register', controller.registerUser);

router.get('/', controller.getAllUsers);


export default router;