const db = require('../../db/models');
import User from './interface';

export async function registerUser(user: User) {
    const newuser = await db.User.create({
      firstName: user.firstName,
      lastName: user.lastName,
      email: user.email,
      password: user.password
    });
    return newuser;
}

export async function findUserByEmail(email: string) {
    const user = await db.User.findOne({
        where: {
          email: email
        }
    })
    return user;
}

export async function getAllUsers() {
    const users = await db.User.findAll();
    return users;
}

export async function addStripeCustomer(user_id: number, stripe_customer: string) {
  const updatedUser = await db.User.update(
    { stripeCustomer: stripe_customer },
    { where: { id: user_id } }
  );
  return updatedUser;
}

export async function addStripeSource(user_id: number, stripe_source: string) {
  const updatedUser = await db.User.update(
    { stripeSource: stripe_source },
    { where: { id: user_id } }
  );
  return updatedUser;
}

export async function addTransaction(transaction_id: string, user_id: number, order_id: number) {
  const transaction = await db.Transaction.create(
    { 
      orderId: order_id,
      userId: user_id,
      transactionId: transaction_id
    },
  );
  return transaction;
}

export async function findUserById(id: number) {
const user = await db.User.findOne({
    where: {
      id: id
    }
})
return user;
}