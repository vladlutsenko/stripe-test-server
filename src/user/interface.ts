export default interface User {
    firstName: string,
    lastName: string,
    email: string,
    password: string,
    stripeCustomer?: string,
    stripeSource?: string
  }