import express from 'express';
import bodyParser from 'body-parser';
import errorHandler from './middlewares/errorHandler'
import jwtVerifier from './middlewares/jwtVerifier'
import cors from 'cors';

import userRouter from './user/router'
import orderRouter from './order/router'

const app = express();
const port = process.env.SERVER_PORT;

app.use(bodyParser.json());
app.use(cors());

app.use(express.static('static'));

/* app.use(jwtVerifier); */

app.use('/order', orderRouter);
app.use('/user', userRouter);

app.get( "/", ( req, res ) => {
    res.send('Hello world!');
});

app.use(errorHandler);

app.listen( port, () => {
    console.log( `server started at http://localhost:${ port }` );
} );