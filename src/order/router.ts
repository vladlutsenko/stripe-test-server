import express from 'express';
import * as controller from './controller'

const router = express.Router();


router.post('/payment', controller.handlePaymentWithToken);


export default router;