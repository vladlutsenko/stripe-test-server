import express from 'express';
const db = require('../../db/models');
import * as service from './service';
import HttpException from '../exceptions/HttpException';


export async function handlePaymentWithToken(req: express.Request, res: express.Response, next: express.NextFunction) {
  try {
    const { body } = req;
    const response = service.processPayment(body.order_id, body.user_id, body.token, body.amount);
    res.status(200).send(response);
  } catch (error) {
    next(new HttpException(400, 'Payment error'));
  }
}


