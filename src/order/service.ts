import * as userModel from '../user/model';
import * as stripe from '../util/stripe';

import User from '../user/interface';


export async function processPayment(order_id:number, user_id:number, token:string, amount:number) {
    const user = await userModel.findUserById(user_id) as User;

    let stripeCustomer;
    if (!user.stripeCustomer) {
        stripeCustomer = (await stripe.createCustomer(user.email)).id;
        userModel.addStripeCustomer(user_id, stripeCustomer);
    }
    else{
        stripeCustomer = user.stripeCustomer;
    }

    console.log(`stripe customer: ${stripeCustomer}`);

    let stripeSource;
    if (!user.stripeSource) {
        stripeSource = (await stripe.createSource(stripeCustomer, token)).id;
        userModel.addStripeSource(user_id, stripeSource);
    }
    else{
        stripeSource = user.stripeSource;
    }

    console.log(`stripe source: ${stripeSource}`);

    const charge = await stripe.payForOrderWithSource(stripeCustomer, stripeSource, order_id, user_id, amount);
    const charge_id = charge.id;
    userModel.addTransaction(charge_id, user_id, order_id);

    console.log(`stripe charge: ${charge}`);

    return charge;
}