const stripe = (require('stripe')(process.env.STRIPE_SECRET_KEY));

export async function payForOrderWithToken(order_id:number, user_id:number, token:string, amount:number) {
    return stripe.charges.create({
        source: token,
        amount,
        currency: 'usd',
        description: `user_id: ${user_id} paid ${amount} for order - ${order_id}`,
        customer: ''
    });
}

export async function payForOrderWithSource(customer:string, source:string, order_id:number, user_id:number, amount:number){
    const charge = await stripe.charges.create({
        source: source,
        amount,
        currency: 'usd',
        description: `user_id: ${user_id} paid ${amount} for order - ${order_id}`,
        customer: customer
      });
      return charge;
}

export async function createSource(customer_id:string, token:string) {
    const source = await stripe.customers.createSource(
            customer_id,
            {source: token}
        );
    return source;
}

export async function createCustomer(email:string) {
    const customer =  await stripe.customers.create(
            {email: email}
        );
    return customer;
}